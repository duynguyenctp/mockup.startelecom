$(document).ready(function(){
  $("#header").load('/includes/header.html');
  $("#mobile_plan-section").load('/includes/index/mobile-plan-section.html');
  $("#pre_paid-section").load('/includes/index/pre-paid-section.html');
  $("#product_service-section").load('/includes/index/product-service-section.html');
  $("#footer").load('/includes/footer.html');
});